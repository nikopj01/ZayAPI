﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZayAPI.ApplicationCore.Entities
{
    public class Stock
    {
        public string Size { get; set; }
        public string Colour { get; set; }
        public int Quantity { get; set; }
    }
}
