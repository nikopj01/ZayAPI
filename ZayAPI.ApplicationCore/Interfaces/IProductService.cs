﻿using ZayAPI.ApplicationCore.Entities;

namespace ZayAPI.ApplicationCore.Interfaces
{
    public interface IProductService
    {
        IEnumerable<Product> GetProducts(CancellationToken token);
        void AddProduct(Product product, CancellationToken token);
        void DeleteProduct(string keyWord, CancellationToken token);
    }
}
