﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZayAPI.ApplicationCore.Interfaces;
using ZayAPI.Infrastructure.Services;

namespace ZayAPI.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IProductService, ProductService>();
            services.AddSingleton<IMongoDbFactory>(new MongoDbFactory(configuration.GetSection("MongoDB:ConnectionString").Value));
            return services;
        }
    }
}
