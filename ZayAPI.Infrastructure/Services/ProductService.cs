﻿using MongoDB.Driver;
using ZayAPI.ApplicationCore.Entities;
using ZayAPI.ApplicationCore.Interfaces;

namespace ZayAPI.Infrastructure.Services
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _product;

        public ProductService(IMongoDbFactory mongoDbFactory)
        {
            _product = mongoDbFactory.GetCollection<Product>("Zay", "Products");
        }

        public IEnumerable<Product> GetProducts(CancellationToken token)
        {
            return _product.Find(x => true).ToList();
        }

        public void AddProduct(Product product, CancellationToken token)
        {
            _product.InsertOne(product);
        }

        public void DeleteProduct(string keyWord, CancellationToken token)
        {
            _product.FindOneAndDelete(keyWord);
        }
    }
}
