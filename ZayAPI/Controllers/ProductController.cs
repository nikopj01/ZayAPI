﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using ZayAPI.ApplicationCore.Entities;
using ZayAPI.ApplicationCore.Interfaces;

namespace ZayAPI.Controllers
{
    [ApiController]
    //[Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;

        public ProductController(ILogger<ProductController> logger, IProductService productService)
        {
            _productService = productService;
            _logger = logger;
        }

        [HttpGet("Product/GetProducts")]
        public async Task<IEnumerable<Product>> GetProducts(CancellationToken token)
        {
            return _productService.GetProducts(token);
        }

        [HttpPost("Product/AddProduct")]
        public async Task<IActionResult> AddProduct(CancellationToken token, Product product)
        {
            _productService.AddProduct(product, token);
            return Ok();
        }

        [HttpDelete("Product/DeleteProduct")]
        public async Task<IActionResult> DeleteProduct(CancellationToken token, string keyword)
        {
            _productService.DeleteProduct(keyword, token);
            return Ok();
        }
    }
}
